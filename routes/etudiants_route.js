var express = require('express');
var router = express.Router();
const etudiants_controller = require("../controllers/etudiants_controller");

/* GET home page. */
router.get('/', etudiants_controller.accueil);
router.get('/ajouter', etudiants_controller.ajouter_etudiant);
router.get('/valider', etudiants_controller.valider_etudiant);

router.post("/", etudiants_controller.sauvegarder);

/*
router.get('/offres', function(req, res, next) {
  res.render('etudiants/offres', { title: 'Express' });
});
*/


/*
router.get('/etudiants', etudiants_controller.lister_etudiants);

router.get('/partenaires', function(req, res, next) {
  res.render('etudiants/partenaires', { title: 'Express' });
});

router.get('/parametres', function(req, res, next) {
  res.render('etudiants/parametres', { title: 'Express' });
});
*/

module.exports = router;